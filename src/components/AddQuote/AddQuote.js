import React, {useState} from 'react';
import axiosApi from "../../axiosApi";

const AddQuote = () => {
	const [quote, setQuote] = useState({
		category: '',
		author: '',
		text: ''
	});

	const options = [
		{label: "", value: ""},
		{label: "Star Wars", value: "star-wars"},
		{label: "Motivational", value: "motivational"},
		{label: "Famous people", value: "famous-people"},
		{label: "Saying", value: "saying"},
		{label: "Humour", value: "humour"},
	];

	const onChangeForm = e => {
		const {name, value} = e.target;

		setQuote(prev => ({
			...prev,
			[name]: value
		}));
	};

	const addQuote = async e => {
		e.preventDefault();

		try {
			await axiosApi.post('/quotes.json', quote);
		} finally {
			setQuote(prev => ({
				...prev,
				category: '',
				author: '',
				text: ''
			}));
		}
	};

	return (
		<div className="container">
			<div className="form-block">
				<h2>Submit new quote</h2>
				<form onSubmit={addQuote}>
					<p>Category</p>
					<select name="category" value={quote.category} onChange={onChangeForm}>
						{options.map((option) => (
							<option key={option.value} value={option.value}>{option.label}</option>
						))}
					</select>
					<div className="form-row">
						<p>Author</p>
						<input
							name="author"
							value={quote.author}
							onChange={onChangeForm}/>
					</div>
					<div className="form-row">
						<p>Quote text</p>
						<textarea
							name="text"
							value={quote.text}
							onChange={onChangeForm}
						/>
					</div>
					<button type="submit">Save</button>
				</form>
			</div>
		</div>
	);
};

export default AddQuote;