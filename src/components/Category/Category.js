import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import Quote from "../Quote/Quote";

const Category = ({match, history}) => {
	const [category, setCategory] = useState(null);
	const [quotesId, setQuotesId] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			const response = await axiosApi.get(`/quotes.json?orderBy="category"&equalTo="${match.params.id}"`);

			if (response.data) {
				setQuotesId(Object.keys(response.data));

				const promises = Object.keys(response.data).map(ID => {
					return axiosApi.get(`/quotes/${ID}.json`);
				});

				const results = await Promise.all(promises);
				setCategory(results);
			}
		};

		fetchData().catch(console.error);
	}, [match.params.id]);

	const removeQuote = async (e, quoteId) => {
			await axiosApi.delete(`/quotes/${quoteId}.json`);
	};

	const editQuote = (e, quoteId, i) => {
		const params = new URLSearchParams(category[i].data);
		history.push(`/quotes/${quoteId}/edit?` + params.toString());
	};

	return category && (
		<div>
			{category.map((quote, i) => (
				<Quote
					key={quote.data.author}
					id={quotesId[i]}
					idBlock={i}
					text={quote.data.text}
					author={quote.data.author}
					deleteQuote={removeQuote}
					editQuote={editQuote}
				/>
			))}

		</div>
	);
};

export default Category;