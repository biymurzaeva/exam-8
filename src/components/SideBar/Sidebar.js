import React, {useState} from 'react';
import './SideBar.css';
import {NavLink} from "react-router-dom";

const Sidebar = () => {
	const [categories] = useState([
		{title: 'All', id: 'all'},
		{title: 'Star Wars', id: 'star-wars'},
		{title: 'Motivational', id: 'motivational'},
		{title: 'Famous people', id: 'famous-people'},
		{title: 'Saying', id: 'saying'},
		{title: 'Humour', id: 'humour'},
	]);

	return (
		<nav className="sidebarNav">
			<ul>
				{categories.map(category => (
					<li key={category.id}>
						<NavLink to={'/quotes/' + category.id}>
						  {category.title}
					  </NavLink>
					</li>
				))}
			</ul>
		</nav>
	);
};

export default Sidebar;