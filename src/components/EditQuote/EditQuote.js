import React, {useState} from 'react';
import axiosApi from "../../axiosApi";

const EditQuote = ({location, match, history}) => {
	const parseParam = () => {
		const params = new URLSearchParams(location.search);
		return Object.fromEntries(params);
	};

	const [quote, setQuote] = useState(parseParam());

	const options = [
		{label: "", value: ""},
		{label: "Star Wars", value: "star-wars"},
		{label: "Motivational", value: "motivational"},
		{label: "Famous people", value: "famous-people"},
		{label: "Saying", value: "saying"},
		{label: "Humour", value: "humour"},
	];

	const onChangeForm = e => {
		const {name, value} = e.target;

		setQuote(prev => ({
			...prev,
			[name]: value
		}));
	};

	const updateQuote = async e => {
		e.preventDefault();

		try {
			await axiosApi.put(`/quotes/${match.params.id}.json`, quote);
		} finally {
			history.replace('/');
		}
	};

	return (
		<div className="container">
			<div className="form-block">
				<h2>Submit new quote</h2>
				<form onSubmit={updateQuote}>
					<p>Category</p>
					<select name="category" value={quote.category} onChange={onChangeForm}>
						{options.map((option) => (
							<option key={option.value} value={option.value}>{option.label}</option>
						))}
					</select>
					<div className="form-row">
						<p>Author</p>
						<input
							name="author"
							value={quote.author}
							onChange={onChangeForm}/>
					</div>
					<div className="form-row">
						<p>Quote text</p>
						<textarea
							name="text"
							value={quote.text}
							onChange={onChangeForm}
						/>
					</div>
					<button type="submit">Update</button>
				</form>
			</div>
		</div>
	);
};

export default EditQuote;