import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
	return (
		<div className="container">
			<header className="header">
				<h2>Quotes central</h2>
				<ul>
					<NavLink to={"/"}>
						<li>Quotes</li>
					</NavLink>
					<NavLink to={"/add-quote"}>
						<li>Submit new quote</li>
					</NavLink>
				</ul>
			</header>
		</div>
	);
};

export default Header;