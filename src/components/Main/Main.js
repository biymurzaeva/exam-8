import React from 'react';
import Sidebar from "../SideBar/Sidebar";
import './Main.css';
import Quotes from "../Quotes/Quotes";
import {Route, Switch} from "react-router-dom";

const Main = () => {
	return (
		<div className="container">
			<div className="main-block">
				<div className="sidebar">
					<Sidebar/>
				</div>
					<div className="content">
						<Switch>
							<Route path="/" exact
								render={props => (
									<Quotes {...props} />
								)}
							/>

						</Switch>
					</div>
			</div>
		</div>
	);
};

export default Main;