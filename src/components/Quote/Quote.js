import React from 'react';
import './Quote.css';

const Quote = props => {
	return (
		<div className="quote" >
			<p>{props.text}</p>
			<p>- {props.author}</p>
			<div className="btn-block">
				<button onClick={e => props.deleteQuote(e.currentTarget, props.id)}>Delete</button>
				<button onClick={e => props.editQuote(e.currentTarget, props.id)}>Edit</button>
			</div>
		</div>
	);
};

export default Quote;