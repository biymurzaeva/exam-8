import React, {useEffect, useState} from 'react';
import Quote from "../Quote/Quote";
import axiosApi from "../../axiosApi";

const Quotes = ({history}) => {
	const [quotes, setQuotes] = useState(null);
	const [quotesId, setQuotesId] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			const response = await axiosApi.get(`/quotes.json`);

			if (response.data) {
				setQuotesId(Object.keys(response.data));

				const promises = Object.keys(response.data).map(ID => {
					return axiosApi.get(`/quotes/${ID}.json`);
				});

				const results = await Promise.all(promises);
				setQuotes(results);
			}
		};

		fetchData().catch(console.error);
	}, []);

	const deleteQuote = async (e, quoteId) => {
		await axiosApi.delete(`/quotes/${quoteId}.json`);
	};

	const editQuote = (e, quoteId, i) => {
		const params = new URLSearchParams(quotes[i].data);
		history.push(`/quotes/${quoteId}/edit?` + params.toString());
	};

	return quotes && (
		<div>
			{quotes.map((quote, i) => (
				<Quote
					key={quote.data.author}
					id={quotesId[i]}
					idBlock={i}
					text={quote.data.text}
					author={quote.data.author}
					deleteQuote={deleteQuote}
					editQuote={editQuote}
				/>
			))}
		</div>
	);
};

export default Quotes;