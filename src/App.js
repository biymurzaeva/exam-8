import {Route, Switch} from "react-router-dom";
import './App.css';
import Header from "./components/Header/Header";
import AddQuote from "./components/AddQuote/AddQuote";
import Main from "./components/Main/Main";
import EditQuote from "./components/EditQuote/EditQuote";
import React from "react";
import Category from "./components/Category/Category";

function App() {
  return (
    <div className="container">
      <Header/>
      <Switch>
        <Route path="/" exact component={Main}/>
        <Route path="/add-quote" component={AddQuote}/>
        <Route path="/quotes/:id/edit" component={EditQuote}/>
        <Route path="/quotes/:id" component={Category}/>

        <Route render={() => <h1>Not Fount</h1>}/>
      </Switch>
    </div>
  );
}

export default App;
