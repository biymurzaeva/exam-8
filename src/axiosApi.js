import axios from 'axios';

const axiosApi = axios.create({
	baseURL: 'https://exam-work-project-default-rtdb.firebaseio.com'
});

export default axiosApi;